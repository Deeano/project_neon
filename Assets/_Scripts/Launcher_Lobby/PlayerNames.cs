﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerNames : Photon.PunBehaviour
{
    public GameObject player1;
    public GameObject player2;
    public GameObject player3;
    public GameObject player4;

    public Text[] playerLabels;

    public GameObject playerReadyButton;

    public static int playerWhoJoined = 1;

    public int playerOrder;

    //private static PhotonView ScenePhotonView;

    //private PhotonView myPhotonView;

    // Use this for initialization
    void Start ()
    {
        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            playerLabels[i].GetComponent<Text>().text = "Player "+ (i+1).ToString() + ":" + PhotonPlayer.Find(i+1).name;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
    }

    public override void OnJoinedRoom()
    {
    }

    public override void OnPhotonPlayerConnected(PhotonPlayer player)
    {
        playerWhoJoined++;

        for (int i = 0; i < PhotonNetwork.playerList.Length; i++)
        {
            playerLabels[i].GetComponent<Text>().text = "Player " + (i + 1).ToString() + ":" + PhotonPlayer.Find(i + 1).name;
        }
    }
}
