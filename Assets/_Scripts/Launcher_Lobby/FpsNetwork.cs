﻿using UnityEngine;
using System.Collections;

public class FpsNetwork : Photon.MonoBehaviour {

    public GameObject cameraScriptObject;
    UnityStandardAssets.Characters.FirstPerson.FirstPersonController controllerScript;

    // Use this for initialization
    void Awake ()
    {
        controllerScript = GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>();

        if (photonView.isMine)
        {
            //MINE: local player, simply enable the local scripts
            cameraScriptObject.SetActive(true);
            controllerScript.enabled = true;
        }
        else
        {
            cameraScriptObject.SetActive(false);

            controllerScript.enabled = false;
            //controllerScript.isControllable = false;
        }

        gameObject.name = gameObject.name + photonView.viewID;
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //We own this player: send the others our data
          //  stream.SendNext((int)controllerScript._characterState);
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else
        {
            //Network player, receive data
           // controllerScript._characterState = (CharacterState)(int)stream.ReceiveNext();
            correctPlayerPos = (Vector3)stream.ReceiveNext();
            correctPlayerRot = (Quaternion)stream.ReceiveNext();
        }
    }

    private Vector3 correctPlayerPos = Vector3.zero; //We lerp towards this
    private Quaternion correctPlayerRot = Quaternion.identity; //We lerp towards this

    void Update()
    {
        if (!photonView.isMine)
        {
            //Update remote player (smooth this, this looks good, at the cost of some accuracy)
            transform.position = Vector3.Lerp(transform.position, correctPlayerPos, Time.deltaTime * 5);
            transform.rotation = Quaternion.Lerp(transform.rotation, correctPlayerRot, Time.deltaTime * 5);
        }
    }
}
