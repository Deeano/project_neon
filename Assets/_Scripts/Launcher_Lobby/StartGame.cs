﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGame : Photon.PunBehaviour
{
    public bool readyCheck = false;
    //public int playersJoined = 0;
    public int playersReady = 0;

    public GameObject startButton;
    public GameObject toggleP1;
    public GameObject toggleP2;
    public GameObject toggleP3;
    public GameObject toggleP4;

    public GameObject playerCamera;

    public bool gameStarted = false;

    void Awake()
    {
        playersReady = 0;
        //PhotonNetwork.Instantiate(this.playerCamera.name, transform.position, Quaternion.identity, 0);
        Debug.Log(PhotonNetwork.room.playerCount);
        Debug.Log(playersReady);
    }

    // Update is called once per frame
    void Update()
    {
        if (gameStarted == false)
        {
            if (PhotonNetwork.room.playerCount == playersReady)
            {
                gameStarted = true;
                PhotonNetwork.LoadLevel(2);
                Debug.Log("called");
            }
        }
    }

    public void StartGameNow()
    {
        playersReady++;
        //startButton.SetActive(false);
        Debug.Log("StartGameNowCalled");
        Debug.Log(PhotonNetwork.room.playerCount);
        Debug.Log(playersReady);
    }
}


