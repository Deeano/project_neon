﻿using UnityEngine;
using System.Collections;

public class RespawnCampDetection : MonoBehaviour {

	public bool camped = false;

	/*void OnTriggerEnter(Collider collide) {

		if (collide.gameObject.tag == "Player") {
			camped = true;
		}
	}*/

	void OnTriggerStay(Collider collide) {
		if (collide.gameObject.tag == "Player") {
			camped = true;
		}
		else {
			camped = false;
		}
	}

	void OnTriggerExit(Collider collide) {

		if (collide.gameObject.tag == "Player") {
			camped = false;
		}
	}
}
