﻿using UnityEngine;
using System.Collections;
//using UnityStandardAssets.Characters.FirstPerson;
//Important usingness!!!

public class Player : Photon.MonoBehaviour {

	public int maxHealth;
	public static int currentHealth;
	PlayerDeath pDScript;
    //public GameObject movementScript;


	// Use this for initialization
	void Start () {
	
		currentHealth = maxHealth;
		pDScript = this.gameObject.GetComponent<PlayerDeath>();
	}

	public void RecieveDamage(int amt)
	{
		if (currentHealth >= 1) 
		{
			currentHealth -= amt;
		}

		if(currentHealth <= 0)
		{
			//print("You dead");
			pDScript.PlayerDead();
			//player was in minus when you died. This stops it. 
			currentHealth = 0;
		}
	}

   /* void OnDeath ()
    {
        movementScript = this.gameObject.GetComponent<FirstPersonController>();
        movementScript.SetActive (false);
    } */

	public void FullHealth () {
		currentHealth = maxHealth;
	}
}
