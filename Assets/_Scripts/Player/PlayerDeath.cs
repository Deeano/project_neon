﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerDeath : MonoBehaviour {

	public GameObject meshBody, collideBody, attacks;
	GameObject[] respawns;
	Player pScript;


	void Start() {
		pScript = this.gameObject.GetComponent<Player>();
	}

	public void PlayerDead (){
		print("You dead");
		this.gameObject.GetComponent<FirstPersonController>().enabled = false;
		this.gameObject.GetComponent<CharacterController>().enabled = false;
		meshBody.gameObject.SetActive (false);
		collideBody.gameObject.SetActive (true);
		attacks.gameObject.SetActive (false);
		StartCoroutine ("Countdown");
	}

	IEnumerator Countdown() { 
		print("counting");

		yield return new WaitForSeconds(5f);
		Respawn();
	}

	void Respawn() {
		
		respawns = GameObject.FindGameObjectsWithTag("Respawn");
		print(respawns.Length);

		GameObject furthest = null;
		float distance = 0;
		Vector3 position = transform.position;

		foreach (GameObject point in respawns) {
			/*if (!point.gameObject.GetComponent<RespawnCampDetection>().camped) {
				this.gameObject.transform.position = point.gameObject.transform.position;
				this.gameObject.GetComponent<CharacterController>().enabled = true;
				this.gameObject.GetComponent<FirstPersonController>().enabled = true;
				meshBody.gameObject.SetActive (true);
				collideBody.gameObject.SetActive (false);
				attacks.gameObject.SetActive (true);
				pScript.FullHealth();
			}*/

			Vector3 diff = point.transform.position - position;
			float curDistance = diff.sqrMagnitude;
			if(curDistance > distance) {
				furthest = point;
				distance = curDistance;
			}

		}

		this.gameObject.transform.position = furthest.gameObject.transform.position;
		this.gameObject.GetComponent<CharacterController>().enabled = true;
		this.gameObject.GetComponent<FirstPersonController>().enabled = true;
		meshBody.gameObject.SetActive (true);
		collideBody.gameObject.SetActive (false);
		attacks.gameObject.SetActive (true);
		pScript.FullHealth();
		print("respawned");
	}
}
