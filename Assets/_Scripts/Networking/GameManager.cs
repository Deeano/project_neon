﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : Photon.MonoBehaviour
{
    #region Public Variables

    static public GameManager Instance;

    [Tooltip("The prefab to use for representing the player")]
    public GameObject playerPrefab;

    public Transform[] SpawnPoints;

    #endregion

    void Awake()
    {
        Instance = this;

        Debug.Log("Game Manager - Start");

        // We're in a room. spawn a character for the local player. it gets synced by using PhotonNetwork.Instantiate
        //SpawnPoint(Random.Range(0, PhotonNetwork.room.playerCount);

        //PhotonNetwork.Instantiate(this.playerPrefab.name, SpawnPoint.position, Quaternion.identity, 0);

        PhotonNetwork.Instantiate(this.playerPrefab.name, SpawnPoints[PhotonNetwork.player.ID - 1].position, Quaternion.identity, 0);     
    }
}