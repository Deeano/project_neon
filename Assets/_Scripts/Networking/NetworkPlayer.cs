﻿using UnityEngine;
using System.Collections;

public class NetworkPlayer : Photon.MonoBehaviour
{
    public GameObject myCamera;

    bool isAlive = true;
    Vector3 trackPosition;
    Quaternion trackRotation;
    float lerpSmoothing = 0.01f;

	// Use this for initialization
	void Awake ()
    {
	    if (photonView.isMine)
        {
            myCamera.SetActive(true);
            GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = true;
            //GetComponent<PlayerMovement>().enabled = true;
            Debug.Log("woo");
        }
        else
        {
            myCamera.SetActive(false);
           
            GetComponent<UnityStandardAssets.Characters.FirstPerson.FirstPersonController>().enabled = false;
            //GetComponent<PlayerMovement>().enabled = false;
            // StartCoroutine("Alive");
        }
	}
	
    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else
        {
            trackPosition = (Vector3)stream.ReceiveNext();
            trackRotation = (Quaternion)stream.ReceiveNext();
        }
    }
    
    IEnumerator Alive ()
    {
        while(isAlive)
        {
            transform.position = Vector3.Lerp(transform.position, trackPosition, Time.deltaTime * lerpSmoothing);
            transform.rotation = Quaternion.Lerp(transform.rotation, trackRotation, Time.deltaTime * lerpSmoothing);

            yield return null;
        }
    }
}
