﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UI : MonoBehaviour 
{
	public Text health;

	// Use this for initialization
	void Start () 
	{
		health.text = "Health: " + Player.currentHealth;
	}
	
	// Update is called once per frame
	void Update () 
	{
		health.text = "Health: " + Player.currentHealth;
	}
}
