﻿using UnityEngine;
using System.Collections;

public class CombatAbilities : Photon.MonoBehaviour
{

    #region Public Variables

    //Grenade
    public GameObject ball;
    private GameObject lightBall;
    public GameObject actualBall;
    public static float lightIntensity;
    public static float damageMultiplier;
    public GameObject grenadePos;

    //Bullet
    public GameObject bullet;
    public GameObject bulletPos;

    //Shield
    public GameObject shield;
    public GameObject shieldPos;
    #endregion

	void Update ()
    {
        if (photonView.isMine)
        {
            //Grenade
            if (Input.GetMouseButtonDown(0))
            {
                lightIntensity = 0;
                //lightBall = Instantiate(ball, grenadePos.transform.position, Quaternion.identity) as GameObject;
                lightBall = PhotonNetwork.Instantiate(ball.name, grenadePos.transform.position, Quaternion.identity, 0) as GameObject;
                lightBall.transform.parent = this.transform;
                damageMultiplier = 0;
            }
            if (Input.GetMouseButton(0))
            {
                lightIntensity += Time.deltaTime * 10;
                Mathf.FloorToInt(lightIntensity);
                lightBall.transform.localScale += new Vector3(0.001f * lightIntensity, 0.001f * lightIntensity, 0.001f * lightIntensity);
                //lightBall.transform.localScale = new Vector3 (Mathf.Clamp (transform.localScale.x, 0.1f, 0.6f), Mathf.Clamp (transform.localScale.y, 0.1f, 0.6f), Mathf.Clamp (transform.localScale.z, 0.1f, 0.6f));
                damageMultiplier = lightIntensity;
            }
            if (Input.GetMouseButtonUp(0))
            {
                //GameObject ball = (GameObject) Instantiate(actualBall, grenadePos.transform.position, Quaternion.identity);
                GameObject grenade = PhotonNetwork.Instantiate(actualBall.name, grenadePos.transform.position, Quaternion.identity, 0) as GameObject;
                grenade.GetComponent<MoveBullet>().mySpawnPoint = transform;
                PhotonNetwork.Destroy(lightBall);
                damageMultiplier = lightIntensity;
            }

            //Bullet
            if (Input.GetMouseButtonDown(1))
            {
                //Instantiate(bullet, bulletPos.transform.position, transform.rotation);
                PhotonNetwork.Instantiate(bullet.name, bulletPos.transform.position, transform.rotation, 0);
            }

            //Shield
            if (Input.GetKeyDown(KeyCode.LeftControl))
            {
                //Instantiate(shield, shieldPos.transform.position, transform.rotation);
                PhotonNetwork.Instantiate(shield.name, shieldPos.transform.position, transform.rotation, 0);
            }
        }
    }
}
