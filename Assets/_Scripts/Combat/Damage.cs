﻿using UnityEngine;
using System.Collections;

public class Damage : Photon.MonoBehaviour
{
	public float damageAmount;

	void OnCollisionEnter(Collision hit)
	{
		if (hit.transform.tag == "Player")
		{
			if(tag == "Bullet")
			{
			print ("hit " + hit.transform.GetComponent<Collider> ().transform.tag);
			damageAmount = 2;
			AddDamage (hit.transform.GetComponent<Player> (), Mathf.RoundToInt (damageAmount));
			}
		}
	}

	void OnTriggerEnter(Collider hit)
	{
		if (hit.transform.tag == "Player")
		{
			if(tag == "Bomb")
			{
				print ("hit " + hit.transform.GetComponent<Collider> ().transform.tag + " with Bomb");
				damageAmount = 2 * CombatAbilities.damageMultiplier;
				print (damageAmount);
				AddDamage (hit.GetComponent<Collider>().GetComponent<Player> (), Mathf.RoundToInt (damageAmount));
			}
		}
	}

	void AddDamage(Player pScript, int dmg)
	{	
		pScript.RecieveDamage (dmg);	
	}
}
