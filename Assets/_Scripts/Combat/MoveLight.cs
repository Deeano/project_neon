﻿using UnityEngine;
using System.Collections;

public class MoveLight : Photon.MonoBehaviour
{
	private float lightSpeed = 2.3f;
	void Update () 
	{
		transform.Translate (0, 0, Time.deltaTime * lightSpeed);
	}
}
