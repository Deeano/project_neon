﻿using UnityEngine;
using System.Collections;

public class ShieldBehaviour : Photon.MonoBehaviour
{
	public bool isGrounded = false;
	public Animator anim;

	void Start()
	{
		isGrounded = false;
		StartCoroutine (ShieldThings ());
	}

	void OnCollisionEnter (Collision other)
	{
		if (other.collider.tag == "floor")
		{
			isGrounded = true;
			//Debug.Log ("hit");
		}
	}

	IEnumerator ShieldThings()
	{
		while(true)
		{
			if (isGrounded == true) 
			{
				//Debug.Log ("working");
				anim.Play ("appear");
				yield return new WaitForSeconds (5f);
                PhotonNetwork.Destroy (this.gameObject);
			}
			yield return null;
		}
	}
}
