﻿using UnityEngine;
using System.Collections;

public class BulletBehaviour : Photon.MonoBehaviour
{
	private float speed = 30.0f;
	public GameObject hole; 

	void Update () 
	{
		transform.Translate (Vector3.forward * Time.deltaTime * speed);
		Destroy (this.gameObject, 2f);
	}

	void OnCollisionEnter(Collision col)
	{
		RaycastHit hit;
		Ray ray = new Ray(transform.position, transform.forward);
		if(Physics.Raycast(ray, out hit, 100f))
		{
			if(hit.collider.tag == "Environment")
			{
				print ("hit the walls");
                //GameObject bulletHole = Instantiate(hole, hit.point + hit.normal * 0.2f, Quaternion.FromToRotation(-Vector3.forward, hit.normal)) as GameObject;
                GameObject bulletHole = PhotonNetwork.Instantiate(hole.name, hit.point + hit.normal * 0.2f, Quaternion.FromToRotation(-Vector3.forward, hit.normal), 0) as GameObject;
                Destroy(bulletHole,3f);
			}
		}
        PhotonNetwork.Destroy(this.gameObject);
	}
	/*void OnCollisionEnter(Collision collision)
	{
		ContactPoint contact = collision.contacts[0];
		//Vector3 worldPos = contact.point;
		Quaternion q = Quaternion.FromToRotation(transform.forward, contact.normal);
		Destroy (this.gameObject);
		Instantiate (hole, collision.gameObject.transform.position + contact.normal * 0.1f, q);
	}*/
}
