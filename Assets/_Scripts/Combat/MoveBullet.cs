﻿using UnityEngine;
using System.Collections;

public class MoveBullet : Photon.MonoBehaviour
{
	public float speed = 100f;
	public GameObject lightPrefab; 
	public Transform mySpawnPoint;

	void Update () 
	{
		transform.Translate(mySpawnPoint.forward * Time.deltaTime * speed);
	}

	void OnCollisionEnter(Collision collision)
	{
		ContactPoint contact = collision.contacts[0];
		Vector3 worldPos = contact.point;
		Quaternion q = Quaternion.FromToRotation (transform.forward, contact.normal);
        PhotonNetwork.Destroy(this.gameObject);
        //GameObject lightLight = Instantiate(lightPrefab, worldPos+contact.normal * 0.01f, q) as GameObject;
        GameObject lightLight = PhotonNetwork.Instantiate(lightPrefab.name, worldPos + contact.normal * 0.01f, q, 0) as GameObject;
		lightLight.GetComponent<Light>().intensity = CombatAbilities.lightIntensity * 0.08f;
		lightLight.GetComponent<Light>().range = CombatAbilities.lightIntensity * 0.2f + 10;
        Destroy(lightLight, 5f);
        //PhotonNetwork.Destroy(lightLight);
        CombatAbilities.lightIntensity = 0;
	}
}
