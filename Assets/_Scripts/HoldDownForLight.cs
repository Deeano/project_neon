﻿using UnityEngine;
using System.Collections;

public class HoldDownForLight : MonoBehaviour 
{
	public float lightIntensity; 

	void Start () 
	{
		lightIntensity = 0;
	}

	void Update () 
	{
		if (Input.GetMouseButton(0))
		{
			lightIntensity += Time.deltaTime * 10;
			Debug.Log(Mathf.FloorToInt(lightIntensity));
		}
		else
		{
			lightIntensity = 0;
		}
	
	}
}
